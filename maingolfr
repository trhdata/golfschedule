maingolfr <-
function(p_reps)
{
	### This function will calculate the best pairings for agolf tournamnet.
	### It is currently setup for 8 golfers in 2 foursomes over 7 days
	### Change ndays to alter the number of days.
	## The basic approach:
	##
	##	Prior to running this sgolf is populated with all possible pairings.
	##      Note for 8 golfers there are only 35 such pairings.
	##
	##	The format of a row from sgolf is an 8-tuple where ther first 4 positions represent foursome 1
	##	
	##	Each of these pairings can be thought to generate an 8x8 "identity" matrix where
	##      m(ij) = 1 if player i plays with player j for a given round.
	##
	##	Function genivec will take a row from sgolf and transform it into an 8-tuple of 0 and 1 where
	##       m(i) = 1 if player i is playing in foursome 1.
	##
	##	Function genmat1 will generate the "identity" matrix from the output of genivec.
	##      
	##	Then the basic form of the function is to select 7 (or ndays) pairings randomly and adding the 7 "identity"
	##      matrices together. This will result in a matrix where row (i,j) represents how many times player i
	##      plays player j for the 7 chosen foursomes. Note this matrix will have all 7 (or ndays on the diagonal)
	##
	##	To get the "best" pairings, take the variance of all the numbers in a matrix. Optimally we want 
	##      all the entries to be as close as possible, so we want the lowest variance over all chosen pairings.
	##
	##
	##	Note for the 7 day problem, a perfect solution is found (using 50000 reps)
	##
	ndays <- 7
	holdmat <- array(dim = c(35, 8, 8))
	##  Generate the 35 matrices
	for(i in 1:35) {
		holdmat[i,  ,  ] <- genmat1(genivec(unlist(sgolf[i,  ])), genivec(unlist(sgolf[i, ])))
	}
	lowind <- c(1:ndays)
	lowvar <- 1000
	lowsum <- matrix(0, 8, 8)
	for(nruns in 1:p_reps) {
		sampg <- sample(1:35, ndays, F)
		sampgolf <- sgolf[sampg,  ]
		##	Now we sample 7 of them
		summat <- matrix(0, 8, 8)
		for(k in sampg) {
			summat <- summat + as.matrix(holdmat[k,  ,  ])
		}
		## Now unlist the sum matrix and calculate variance
		sampvar <- var(as.vector(summat))
		if(sampvar < lowvar) {
			lowvar <- sampvar
			lowind <- sampg
			lowsum <- summat
		}
	}
	#cat(lowvar, fill = T, lowind, fill = T, lowsum)
	#cat(lowvar,fill=T)
	#cat(lowind)
      # Return the 7 days of foursomes
      sgolf[lowind,]
}

